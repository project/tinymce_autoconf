<?php

/**
 * Implementation of hook_menu().
 */
function tinymce_autoconf_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path' => 'admin/settings/tinymce_autoconf',
      'title' => t('TinyMCE -- create default profile'),
      'callback' => 'drupal_get_form',
      'callback arguments' => array('tinymce_autoconf_form', NULL),
      'description' => t('Delivers a secure configuration of the TinyMCE module with a single mouse click.'),
      'access' => user_access('administer tinymce'),
      'type' => MENU_NORMAL_ITEM,
    );
  }
  return $items;
}

/**
 * Creates the TinyMCE automatic configuration page
 * It is one big button and a short description of the button. What could be simpler?
 */
function tinymce_autoconf_form() {
  $form = array();
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create/Restore TinyMCE defaults'),
    '#prefix' => t('This will create a new secure configuration of the TinyMCE module, or restore a previously created one back to how it was originally.'),
    '#suffix' => '<br/>'. t('The following actions will be performed:') . theme('item_list', array(
      t('Gives authenticated users permission to use TinyMCE.'),
      t('Creates/restores a default TinyMCE configuration.'),
      t('Sets Filtered HTML as the default input format and configures it securely.'),
    )),
  );
  return $form;
}

/**
 * Ensures that a role has a permission.
 * @param $rid ID of the role
 * @param $perm Permission to ensure the role has
 */
function tinymce_autoconf_ensure_role_permission($rid, $perm) {
  $result = db_query('SELECT perm FROM {permission} WHERE rid = %d', $rid);
  if ($row = db_fetch_object($result)) {
    $permissions = explode(', ', $row->perm);
  }
  else {
    $permissions = array();
  }
  if (array_search($perm, $permissions) === FALSE) {
    $permissions[] = $perm;
    db_query('DELETE FROM {permission} WHERE rid = %d', $rid);
    db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", $rid, implode(', ', $permissions));
  }
}

/**
 * Sets the roles that have access to a TinyMCE profile
 * @param $profile Name of the TinyMCE profile
 * @param $roles Array containing the role IDs to give access to
 */
function tinymce_autoconf_set_profile_roles($profile, $roles) {
  db_query("DELETE FROM {tinymce_role} WHERE name = '%s'", $profile);
  foreach ($roles as $rid) {
    db_query("INSERT INTO {tinymce_role} VALUES ('%s', %d)", $profile, $rid);
  }
}

/**
 * Sets the settings for a TinyMCE profile
 * @param $profile Name of the TinyMCE profile
 * @param $settings Array containing the new profile settings
 */
function tinymce_autoconf_set_profile_settings($profile, $settings) {
  db_query("DELETE FROM {tinymce_settings} WHERE name = '%s'", $profile);
  db_query("INSERT INTO {tinymce_settings} (name, settings) VALUES ('%s', '%s')", $profile, serialize($settings));
}

/**
 * Creates the TinyMCE automatic configuration page
 * It is one big button and a short description of the button. What could be simpler?
 */
function tinymce_autoconf_form_submit($form_id, $form_values) {
  // 1) Give the "authenticated user" role access rights to use TinyMCE
  tinymce_autoconf_ensure_role_permission(DRUPAL_AUTHENTICATED_RID, 'access tinymce');

  // 2) Create the TinyMCE profile "default" and give "authenticated user" access
  tinymce_autoconf_set_profile_roles('default', array(DRUPAL_AUTHENTICATED_RID));

  // 3) Set the settings for the TinyMCE profile "default"
  tinymce_autoconf_set_profile_settings('default', array(
    'name' => 'default',
    'rids' => array(DRUPAL_AUTHENTICATED_RID),
    'default' => 'true',
    'user_choose' => 'true',
    'show_toggle' => 'true',
    'theme' => 'advanced',
    'language' => 'en',
    'safari_message' => 'false',
    'access' => 1,
    'access_pages' => implode("\n", array(
      'node/*',
      'user/*',
      'comment/*',
    )),
    'buttons' => array(
      'advhr-advhr' => 1,
      'advimage' => 1,
      'advlink' => 1,
      'contextmenu' => 1,
      'default-bold' => 1,
      'default-italic' => 1,
      'default-underline' => 1,
      'default-strikethrough' => 1,
      'default-justifyleft' => 1,
      'default-justifycenter' => 1,
      'default-justifyright' => 1,
      'default-bullist' => 1,
      'default-numlist' => 1,
      'default-outdent' => 1,
      'default-indent' => 1,
      'default-link' => 1,
      'default-unlink' => 1,
      'default-image' => 1,
      'default-cleanup' => 1,
      'default-forecolor' => 1,
      'default-backcolor' => 1,
      'default-sup' => 1,
      'default-sub' => 1,
      'default-removeformat' => 1,
      'default-charmap' => 1,
      'emotions-emotions' => 1,
      'font-fontselect' => 1,
      'paste-pasteword' => 1,
      'table-tablecontrols' => 1,
    ),
    'toolbar_loc' => 'top',
    'toolbar_align' => 'center',
    'path_loc' => 'bottom',
    'resizing' => 'true',
    'block_formats' => 'p,address,pre,h2,h3,h4,h5,h6',
    'verify_html' => 'false',
    'preformatted' => 'false',
    'convert_fonts_to_spans' => 'false',
    'remove_linebreaks' => 'false',
    'apply_source_formatting' => 'true',
    'css_setting' => 'theme',
    'css_path' => '',
    'css_classes' => '',
  ));

  // 4) Set the settings for filtered HTML
  variable_set('filter_default_format', 1);
  variable_set('filter_html_1', 1);
  variable_set('filter_url_length_1', 72);
  variable_set('allowed_html_1', implode(' ', array(
    '<a>',
    '<b>',
    '<address>',
    '<blockquote>',
    '<br>',
    '<caption>',
    '<center>',
    '<code>',
    '<dd>',
    '<del>',
    '<div>',
    '<dl>',
    '<dt>',
    '<em>',
    '<font>',
    '<h2>',
    '<h3>',
    '<h4>',
    '<h5>',
    '<h6>',
    '<hr>',
    '<i>',
    '<img>',
    '<li>',
    '<ol>',
    '<p>',
    '<pre>',
    '<span>',
    '<strong>',
    '<sub>',
    '<sup>',
    '<table>',
    '<tbody>',
    '<td>',
    '<tfoot>',
    '<th>',
    '<thead>',
    '<tr>',
    '<u>',
    '<ul>',
    '<tr>',
  )));
  variable_set('filter_html_help_1', 1);
  variable_set('filter_html_nofollow_1', 0);

  // 5) All done
  drupal_set_message(t('TinyMCE has been configured.'));
}
